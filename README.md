# Network Science

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.di.unipmn.it%2F20024182%2Fns/HEAD?filepath=index.ipynb)

Contenitore del progetto per il modulo di Network Science del corso di Information Retrieval dell'A.A. 2021/22.

## Risorse aggiuntive

Oltre al Notebook Python vi sono 3 file aggiunti:
- `network.gexf` una rappresentazione in formato GEXF realizzata da NetworkX compatibile con Gephi.
- `network.gephi` il progetto realizzato dal file precedente. 
   [ForceAtlas](https://gephi.org/tutorials/gephi-tutorial-layouts.pdf#page=11), sviluppato dal team di Gephi, è stato utilizzato per la disposizione dei nodi.
   Esso non ha una terminazione e si basa sempre sull'idea di minimizzare le forze all'interno della rete così come fa `nx.spring_layout`, che però è basato su [Fruchterman-Reingold](https://gephi.org/tutorials/gephi-tutorial-layouts.pdf#page=13).
   All'interruzione della sua esecuzione, ForceAtlas è stato rieseguito con parametri diversi per [minimizzare la sovrapposizione di nodi](https://gephi.org/tutorials/gephi-tutorial-layouts.pdf#page=31).
   Inoltre i colori delineano quali sono le comunità trovate e la dimensione del nodo permette di individuare immediatamente gli hub.
- `network.pdf` la rete renderizzata esportata in PDF

Ho deciso di utilizzare Gephi perché in questo modo è facile esplorare la rete: in particolare ho potuto mettere in evidenza alcune sue zone, capire come i link collegavano i nodi, ma anche visualizzare la rete senza archi.
Esplorare la rete mi ha ulteriormente confermato ciò che ho scoperto con l'analisi nel Notebook.